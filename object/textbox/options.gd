extends Control

var next_text = []
var next_addValue = []
var next_setValue = []
var parent = null
var started = false;

# Called when the node enters the scene tree for the first time.
func _ready():
	var args = get_parent().last_scene_args
	var buttons = args[0].strings
	next_text = args[1]
	next_addValue = get_parent().valueAdd
	next_setValue = get_parent().valueSet
	var counter = 0
	for c in buttons:
		var button = Button.new()
		button.text = buttons[counter]
		$Panel/GridContainer.add_child(button)
		counter+=1;
		button.connect("pressed", self, "pressButton")
		button.connect("focus_entered", self, "selectButton")
		
	$Panel/GridContainer.get_child(0).grab_focus()
	$AnimationPlayer.play("load")

func initialize():
	started = true

func pressButton():
	if started:
		$AnimationPlayer.play("unload")
		$Option.play()
		
func selectButton():
	var owner = get_focus_owner()
	$Select.play()
		
func destroy():
	var args = get_parent().last_scene_args
	var owner = get_focus_owner()
	var index = owner.get_index()
	if index < next_setValue.size():
		for s in next_setValue[index]:
			Global.setValue(s,next_setValue[index][s])
	if index < next_addValue.size():
		for s in next_addValue[index]:
			Global.addValue(s,next_addValue[index][s])
	Global.loadTextbox(next_text[index])
	parent.queue_free()

func _exit_tree():
	get_parent();
	if get_parent().has_method("freeScene"):
		get_parent().freeScene()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
