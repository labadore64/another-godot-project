extends Control

var textbox_position = -1
var textbox_speed = 1; # textbox display speed
var string_arrays = ["test string", "more text for you to read"]; # array of strings
var started = false
var last_scene = null
var last_scene_args = []
var last_instance = null
var valueSet = []
var valueAdd = []

# Called when the node enters the scene tree for the first time.
func initialize():
	started = true
	resume()

func _ready():
	$AnimationPlayer/Open.play()
	Global.freezePlayer(true)
	$AnimationPlayer.play("enter")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if started:
		if $NinePatchRect/RichTextLabel.visible_characters >= 0:
			$NinePatchRect/RichTextLabel.visible_characters += textbox_speed
			if $NinePatchRect/RichTextLabel.visible_characters > $NinePatchRect/RichTextLabel.text.length():
				$NinePatchRect/RichTextLabel.visible_characters = -1
		if Input.is_action_just_pressed("ui_select"):
			if $NinePatchRect/RichTextLabel.visible_characters >= 0:
				$NinePatchRect/RichTextLabel.visible_characters = -1
			else:
				resume()
		elif Input.is_action_just_pressed("ui_accept"):
			textbox_position = string_arrays.size();
			$NinePatchRect/RichTextLabel.text = string_arrays[textbox_position-1];
			resume()	
		elif Input.is_action_just_pressed("ui_cancel"):
			textbox_position = string_arrays.size();
			$NinePatchRect/RichTextLabel.text = string_arrays[textbox_position-1];
			resume()
		
		if $NinePatchRect/RichTextLabel.visible_characters == -1:
			$NinePatchRect/TextLoaded.visible = true
			if $AnimationPlayer.current_animation != "exit":
				$AnimationPlayer.play("sprite_anim")
		else:
			$NinePatchRect/TextLoaded.visible = false
		
func freeScene():
	last_scene = null
	resume()	
			
func resume():
	if (last_instance == null):
		textbox_position+= 1
		if textbox_position >= string_arrays.size():
			if last_scene != null:
				last_instance = last_scene.instance()
				last_instance.parent = self;
				add_child(last_instance)
				
			else:
				$AnimationPlayer.play("exit")
				$AnimationPlayer/Close.play()
		else:
			$NinePatchRect/RichTextLabel.visible_characters = 0
			$NinePatchRect/RichTextLabel.text = string_arrays[textbox_position];
			if textbox_position > 0:
				$AnimationPlayer/Continue.play()

func destroy():
	Global.freezePlayer(false)
	queue_free()
