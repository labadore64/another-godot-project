extends KinematicBody

# How fast the player moves in meters per second.
export var speed = 1
var steppy = 0;

var frozen = false;
var velocity = Vector3.ZERO

func resetAnimation():
	if $Sprite.animation.find("back") != -1:
		$Sprite.animation = "idle-back"
	else:
		$Sprite.animation = "idle-front"

func _physics_process(delta):
	if !frozen:
		# interact with stuff
		if Input.is_action_just_pressed("ui_select"):
			var areas = $Pivot/Area.get_overlapping_areas()
			if areas.size() > 0:
				areas[0].emit_signal("onInteract")
		
		# movement
		var direction = Vector3.ZERO
		var posi = translation

		if Input.is_action_pressed("move_right"):
			direction.x += 1
			$Sprite.scale.x = 1
			if $Sprite.animation != "walk-front" && $Sprite.animation != "walk-back":
				$Sprite.animation = "walk-front"
		if Input.is_action_pressed("move_left"):
			direction.x -= 1
			$Sprite.scale.x = -1
			if $Sprite.animation != "walk-front" && $Sprite.animation != "walk-back":
				$Sprite.animation = "walk-front"
		if Input.is_action_pressed("move_backward"):
			direction.z += 1
			$Sprite.animation = "walk-front"
		if Input.is_action_pressed("move_forward"):
			direction.z -= 1
			$Sprite.animation = "walk-back"

		# determines sprite and movement
		if direction != Vector3.ZERO:
			direction = direction.normalized()
			$Pivot.look_at(translation + direction, Vector3.UP)
			
		else:
			if $Sprite.animation == "walk-front":
				$Sprite.animation = "idle-front"
			elif $Sprite.animation == "walk-back":
				$Sprite.animation = "idle-back"
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
		velocity.y = 0
		velocity = move_and_slide(velocity, Vector3.UP)
		
		# sound effects
		if velocity != Vector3.ZERO:
			if posi.distance_to(translation) > 0.005:
				var stepcounter = $Sprite.frames.get_animation_speed($Sprite.animation)
				steppy += delta*30;
				
				if steppy > stepcounter:
					steppy = 0
					$Step.pitch_scale = rand_range(0.9,1.15)
					$Step.play()
			else:
				if !$Hit.playing:
					$Hit.play()
