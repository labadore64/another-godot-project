extends Resource

export(Array, String, MULTILINE) var strings
export(PackedScene) var special
export(Array) var special_args
export(Array, Dictionary) var valueSet
export(Array, Dictionary) var valueAdd

func _init(p_strings = [],_special=null,_special_args=[],_valueSet=[], _valueAdd=[]):
	strings = p_strings
	special = _special
	special_args = _special_args
	valueSet = _valueSet
	valueAdd = _valueAdd
