extends Resource

export(String) var name
export(String) var description

func _init(_name = "",_desc=""):
	name = _name
	description = _desc
