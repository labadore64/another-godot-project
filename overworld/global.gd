extends Node

var Player = null

var textbox = preload("res://object/textbox/textbox.tscn")

var stored_values = {}

var inventory = {}

## Methods for managing the player instance
func setPlayer(player):
	Player = player;
	
func freezePlayer(value):
	if Player != null:
		Player.frozen = value
		if value:
			Player.resetAnimation()

## Methods for loading maps

func loadMap(mapname):
	var node = get_node("map_loaded")
	if node != null:
		node.queue_free()
	var map = load("res://overworld/map/"+mapname + ".tscn").instance()
	map.name = "map_loaded"
	get_tree().get_root().get_node("Game").add_child(map)
	setPlayer(map.get_node("Player"))
	freezePlayer(false)

## Methods for textboxes and other universal objects

func loadTextbox(name):
	var text = textbox.instance()
	var textbox_text = load("res://textboxes/" + name + ".tres")
	text.string_arrays = textbox_text.strings;
	text.last_scene = textbox_text.special
	text.last_scene_args = textbox_text.special_args
	text.valueSet = textbox_text.valueSet
	text.valueAdd = textbox_text.valueAdd
	get_tree().get_root().get_node("Game").add_child(text)
	
## Methods for storing in-game values
	
func hasValue(key):
	return stored_values.has(key)
	
func setValue(key,value):
	stored_values[key] = value
		
func addValue(key,value):
	stored_values[key] += value
	
func getValue(key):
	if hasValue(key):
		return stored_values[key]
	else:
		return null

## Methods for inventory

func addItem(item,quantity):
	inventory[item] += quantity
	# this can be used for removing items
	if inventory[item] < 0:
		inventory.erase(item);

func getItemResource(name):
	var item = load("res://items/" + name + ".tres")
	if item == null:
		item = load("res://items/test_item.tres")
	return item;
