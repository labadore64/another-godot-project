extends "res://overworld/prop/TextInteract.gd"


## this is overridden with children.
func on_interact():
	if !Global.hasValue("House"):
		displayTextBox("HouseText")
	else:
		displayTextBox("HouseText5")
