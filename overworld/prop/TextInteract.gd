extends Spatial

signal onInteract;

## this is overridden with children.
func on_interact():
	print("interacted with this object!")

func _on_TextInteract_onInteract():
	on_interact()

func displayTextBox(name):
	Global.loadTextbox(name)
